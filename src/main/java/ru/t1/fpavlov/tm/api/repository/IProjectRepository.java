package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project findById(final String id);

    Project findByIndex(final Integer index);

    void remove(final Project project);

    Project removeById(final String id);

    Project removeByIndex(final Integer index);

}
