package ru.t1.fpavlov.tm.model;

import java.util.UUID;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "".format(" |%40s |%20s |%20s |", this.id, this.name, this.description);
    }

}
